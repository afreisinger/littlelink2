FROM nginx:alpine
RUN rm /etc/nginx/conf.d/*
COPY conf/default.conf conf/health-check.conf /etc/nginx/conf.d/
COPY conf/nginx.conf /etc/nginx/
COPY html/. /usr/share/nginx/html/
